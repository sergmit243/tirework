<?php


use App\Repositories\GoodRepository;
use App\Services\GoodService;

include_once '../vendor/autoload.php';

$goodRepository = new GoodRepository();
$goodService = new GoodService($goodRepository);
echo $goodService->getList();