<?php

use App\Database\Connection;
use App\Database\DBInit;
use App\Database\Seeds\DataLoad;

include_once '../vendor/autoload.php';

$connection = Connection::getInstance();
$dbInit = new DBInit($connection);
$dbInit->execute();
$dataLoad = new DataLoad();
$dataLoad->execute();

echo "Init successful";