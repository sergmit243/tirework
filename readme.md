1.
```text
Перед отправкой баннера создаем запись в бд(id, counter, counterUnique), записываем его id в сессию,
далее смотрим, если в сессии нет этого id То плюсуем counterUnique, всегда плюсуем counter
```


2.
```sql
CREATE TABLE `projects` ( `id` INT NOT NULL , 
                          `title` VARCHAR(150) NOT NULL , 
                          PRIMARY KEY (`id`)) ENGINE = InnoDB;
```

```sql
CREATE TABLE `employee` ( `id` INT NOT NULL AUTO_INCREMENT ,
                                   `name` VARCHAR(10) NOT NULL ,
                                   `type` ENUM('programmer','manager','tester','') NOT NULL ,
                                   PRIMARY KEY (`id`)) ENGINE = InnoDB;
```

```sql
CREATE TABLE `project_employee` ( `employee_id` INT NOT NULL ,
                                  `project_id` INT NOT NULL ,
                                  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                                   INDEX (`employee_id`), INDEX (`project_id`)) ENGINE = InnoDB;
```

```sql
SELECT COUNT(e.id) as count, p.title FROM `project_employee` pe
LEFT JOIN `employee` e ON e.id = pe.employee_id
LEFT JOIN `projects` p ON p.id = pe.project_id
WHERE e.type = 'programmer'
GROUP BY pe.project_id
HAVING count >= 3

```