<?php


namespace App\Core;


interface GoodRepositoryInterface {
    public function findAll() : ?array;
}