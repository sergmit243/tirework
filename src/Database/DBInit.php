<?php


namespace App\Database;


class DBInit {

    /**
     * @var Connection
     */
    private $connection;
    private $data;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function execute()
    {
        $query = <<<SQL
CREATE TABLE IF NOT EXISTS  `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;
        try
        {
            $db = $this->connection->getDB();
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();

            return false;
        }

        $db->exec($query);

        $query = <<<SQL
CREATE TABLE IF NOT EXISTS  `parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) NOT NULL,
  `width` smallint(6) DEFAULT NULL COMMENT 'Ширина',
  `height` smallint(6) DEFAULT NULL COMMENT 'Высота',
  `structure` varchar(50) DEFAULT NULL COMMENT 'Конструкция',
  `diameter` tinyint(4) DEFAULT NULL COMMENT 'Диаметр',
  `index_load` varchar(10) DEFAULT NULL COMMENT 'Индекс нагрузки',
  `speed` varchar(10) DEFAULT NULL COMMENT 'Индекс скорости',
  `parameters` varchar(50) DEFAULT NULL,
  `runflat` varchar(50) DEFAULT NULL,
  `chamber` varchar(50) DEFAULT NULL COMMENT 'Камерность',
  `season` varchar(200) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX `good_id` (good_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;
        $db->exec($query);
    }
}