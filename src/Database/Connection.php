<?php


namespace App\Database;



class Connection {
    private $db = null;
    private static $inst = null;

    private function __construct()
    {
        try
        {
            $this->db = new \PDO("mysql:dbname=tiredb;host=localhost", 'root');
            $this->db->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch (\PDOException $e)
        {
            echo "Ошибка подключения к базе данных";
        }
    }

    public static function getInstance()
    {
        if (empty(self::$db)) {
            self::$inst = new static();
        }
        return self::$inst;
    }

    /**
     * @return \PDO|null
     * @throws \Exception
     */
    public function getDB() {
        if (!empty($this->db))
            return $this->db;
        else
            throw new \Exception("Нет подключения к базе данных");
    }
}