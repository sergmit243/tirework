<?php


namespace App\Database\Seeds;


use App\Database\Connection;

class DataLoad {
    private $data;

    public function __construct()
    {
        $this->data = [
            ['brand'    => 'Toyo', 'name' => 'H08', 'width' => 195, 'height' => 75, 'structure' => 'R',
             'diameter' => 16, 'parameters' => 'C 107/105S', 'runflat' => null, 'chamber' => 'TL',
             'season'   => 'Летние'],
            ['brand'      => 'Pirelli', 'name' => 'Winter SnowControl serie 3', 'width' => 175,
             'height'     => 70, 'structure' => 'R', 'diameter' => 14, 'index_load' => '84', 'speed' => 'T',
             'parameters' => null, 'runflat' => null, 'chamber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'BFGoodrich', 'name' => 'Mud-Terrain T/A KM2', 'width' => 235, 'height' => 85,
             'structure'  => 'R', 'diameter' => 16, 'index_load' => '120/116', 'speed' => 'Q',
             'parameters' => null, 'runflat' => null, 'chamber' => 'TL', 'season' => 'Внедорожные'],
            ['brand'     => 'Pirelli', 'name' => 'Scorpion Ice & Snow', 'width' => 265, 'height' => 45,
             'structure' => 'R', 'diameter' => 21, 'index_load' => '104', 'speed' => 'H', 'chamber' => 'TL',
             'season'    => 'Зимние (нешипованные)'],
            ['brand'      => 'Pirelli', 'name' => 'Winter SottoZero Serie II', 'width' => 245,
             'height'     => 45, 'structure' => 'R', 'diameter' => 19, 'index_load' => '102', 'speed' => 'V',
             'parameters' => 'XL', 'runflat' => 'Run Flat', 'chamber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'Nokian', 'name' => 'Hakkapeliitta R2 SUV/Е', 'width' => 245, 'height' => 70,
             'structure'  => 'R', 'diameter' => 16, 'index_load' => '111', 'speed' => 'R',
             'parameters' => 'XL', 'runflat' => null, 'chamber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'Pirelli', 'name' => 'Winter Carving Edge', 'width' => 225, 'height' => 50,
             'structure'  => 'R', 'diameter' => 17, 'index_load' => '98', 'speed' => 'T',
             'parameters' => 'XL', 'runflat' => null, 'chamber' => 'TL', 'season' => 'Зимние (шипованные)'],
            ['brand'      => 'Continental', 'name' => 'ContiCrossContact LX Sport', 'width' => 255,
             'height'     => 55, 'structure' => 'R', 'diameter' => 18, 'index_load' => '105', 'speed' => 'H',
             'parameters' => 'FR MO', 'runflat' => null, 'chamber' => 'TL', 'season' => 'Всесезонные'],
            ['brand'      => 'BFGoodrich', 'name' => 'g-Force Stud', 'width' => 205, 'height' => 60,
             'structure'  => 'R', 'diameter' => 16, 'index_load' => '96', 'speed' => 'Q',
             'parameters' => 'XL', 'runflat' => null, 'chamber' => 'TL', 'season' => 'Зимние (шипованные)'],
            ['brand'      => 'BFGoodrich', 'name' => 'Winter Slalom KSI', 'width' => 225, 'height' => 60,
             'structure'  => 'R', 'diameter' => 17, 'index_load' => '99', 'speed' => 'S',
             'parameters' => null, 'runflat' => null, 'chamber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'Continental', 'name' => 'ContiSportContact 5', 'width' => 245, 'height' => 45,
             'structure'  => 'R', 'diameter' => 18, 'index_load' => '96', 'speed' => 'W',
             'parameters' => 'SSR', 'runflat' => 'FR', 'chamber' => 'TL', 'season' => 'Летние'],
            ['brand'      => 'Continental', 'name' => 'ContiWinterContact TS 830 P', 'width' => 205,
             'height'     => 60, 'structure' => 'R', 'diameter' => 16, 'index_load' => '92', 'speed' => 'H',
             'parameters' => null, 'runflat' => 'SSR *', 'chumber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'Continental', 'name' => 'ContiWinterContact TS 830 P', 'width' => 225,
             'height'     => 45, 'structure' => 'R', 'diameter' => 18, 'index_load' => '95', 'speed' => 'V',
             'parameters' => 'XL', 'runflat' => 'SSR FR *', 'chumber' => 'TL',
             'season'     => 'Зимние (нешипованные)'],
            ['brand'      => 'Hankook', 'name' => 'Winter I*Cept Evo2 W320', 'width' => 255, 'height' => 35,
             'structure'  => 'R', 'diameter' => 19, 'index_load' => '96', 'speed' => 'V',
             'parameters' => 'XL', 'runflat' => 'TL/TT', 'season' => 'Зимние (нешипованные)'],
            ['brand'      => 'Mitas', 'name' => 'Sport Force+', 'width' => 120, 'height' => 65,
             'structure'  => 'R', 'diameter' => 17, 'index_load' => '56', 'speed' => 'W',
             'parameters' => null, 'chumber' => 'TL', 'season' => 'Летние']
        ];
    }

    public function execute() {
        $db = Connection::getInstance()->getDB();
        foreach ($this->data as $key => $item) {
            $query = <<<SQL
INSERT INTO goods (brand, name) VALUES (:brand, :name)
SQL;
            $stmt = $db->prepare($query);
            $stmt->execute([':brand' => $item['brand'], ':name' => $item['name']]);
            $good_id = $db->lastInsertId();
            $query = <<<SQL
INSERT INTO `parameters`(`goods_id`, `width`, `height`, `structure`, `diameter`, `index_load`, `speed`, `parameters`, `runflat`, `chamber`, `season`)
VALUES ($good_id, :width, :height, :structure, :diameter, :index_load, :speed, :parameters, :runflat, :chamber, :season)
SQL;
            $stmt = $db->prepare($query);
            $stmt->execute([':width' => $item['width'] ?? null,
                            ':height' => $item['height'] ?? null,
                            ':structure' => $item['structure'] ?? null,
                            ':diameter' => $item['diameter'] ?? null,
                            ':index_load' => $item['index_load'] ?? null,
                            ':speed' => $item['speed'] ?? null,
                            ':parameters' => $item['parameters'] ?? null,
                            ':runflat' => $item['runflat'] ?? null,
                            ':chamber' => $item['chamber'] ?? null,
                            ':season' => $item['season'] ?? null
                           ]);
        }
    }
}