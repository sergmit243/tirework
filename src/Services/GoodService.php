<?php


namespace App\Services;


use App\Core\GoodRepositoryInterface;
use App\Models\Good;

class GoodService {

    /**
     * @var GoodRepositoryInterface
     */
    private $goodRepository;

    public function __construct(GoodRepositoryInterface $goodRepository)
    {

        $this->goodRepository = $goodRepository;
    }

    public function getList(): string {
        $goods = $this->goodRepository->findAll();
        if (empty($goods)) {
            return '';
        }
        $list = "<ul>";
        /** @var Good $good */
        foreach ($goods as $good) {
            $error = $good->validate() ? '' : 'style="color: red"';
            $list .= "<li $error>$good</li>";
        }
        $list .= "</ul>";
        return $list;
    }
}