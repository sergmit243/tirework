<?php


namespace App\Models;


use App\Core\BaseModel;

/**
 * Class Good
 *
 * @params  string brand
 * @params  string name
 * @params  int width
 * @params  int height
 * @params  string structure
 * @params  int diameter
 * @params  string index_load
 * @params  string speed
 * @params  string parameters
 * @params  string runflat
 * @params  string chamber
 * @params  string season
 *
 * @package App\Models
 */
class Good extends BaseModel {
    private $brand = '';
    private $name = '';
    private $width = '';
    private $height = '';
    private $structure = '';
    private $diameter = '';
    private $index_load = '';
    private $speed = '';
    private $parameters = '';
    private $runflat = '';
    private $chamber = '';
    private $season = '';
    private $requiredParams = ['brand', 'name', 'width', 'height', 'structure', 'diameter', 'index_load', 'speed',
                               'season'];

    /**
     * @return string
     */
    public function getTitle()
    {
        return "{$this->brand} {$this->name}";
    }

    public function getParams(): string
    {
        $title = $this->getTitle();

        return "$title {$this->width}/{$this->height} {$this->structure}{$this->diameter} {$this->index_load}{$this->speed} {$this->parameters} {$this->runflat} {$this->chamber} {$this->season}";
    }

    public function __toString()
    {
        return $this->getParams();
    }

    /**
     * Validate params
     * @return bool
     */
    public function validate()
    {
        foreach ($this->requiredParams as $param)
        {
            if (empty($this->{$param}))
                return false;
        }

        return true;
    }
}