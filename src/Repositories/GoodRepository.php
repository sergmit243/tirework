<?php


namespace App\Repositories;


use App\Core\GoodRepositoryInterface;
use App\Database\Connection;
use App\Models\Good;

class GoodRepository implements GoodRepositoryInterface {
    private $db;
    public function __construct()
    {
        $this->db = Connection::getInstance()->getDB();
    }

    public function findAll(): ?array
    {
        $query = <<<SQL
SELECT * FROM goods g
LEFT JOIN parameters p ON p.good_id = g.id
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $goods = $stmt->fetchAll(\PDO::FETCH_CLASS, Good::class);
        if (empty($goods)) {
            return null;
        }
        return $goods;
    }
}