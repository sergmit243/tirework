<?php


namespace App\Repositories;


use App\Core\GoodRepositoryInterface;

class GoodFileRepository implements GoodRepositoryInterface {

    public function findAll() :?array
    {
        return [];
    }
}